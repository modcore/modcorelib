pub use std::ffi::c_void;
pub type Fn_modinit_notify = unsafe extern "C"
        fn(*mut c_void, //appstatedata
           NotifStateRaw) -> NotifHandleRaw;
pub type Fn_register_mod = unsafe extern "C"
        fn(*mut c_void, //appstatedata
           ModStateRaw) -> ModHandleRaw;
pub type Fn_notify_initmod = unsafe extern "C" fn(*mut c_void, *const ModStateRaw); // notify_initmod(customdata, mod)
pub type Fn_notify_deinitmod = unsafe extern "C" fn(*mut c_void, *const u8,usize); // notify_deinitmod(customdata, name_ptr,name_size)
pub type Fn_deinit = unsafe extern "C" fn(*mut c_void);
pub type Fn_get_features = unsafe extern "C" fn(*mut c_void) -> FeatureArrayRaw;
pub type Fn_call_func = unsafe extern "C" fn(*mut c_void, *const u8,usize, SomeValueRaw) -> SomeValueRaw;
pub type Fn_dropfn = unsafe extern "C" fn(*mut c_void);
pub type Fn_modstate_clone = unsafe extern "C" fn(*const ModStateRaw) -> ModStateRaw;
pub type Fn_appstate_clone = unsafe extern "C" fn (*const AppStateRaw) -> AppStateRaw;

#[repr(C)]
pub struct AppStateRaw {
    pub data: *mut c_void,
    pub modinit_notify: Fn_modinit_notify,
    pub register_mod: Fn_register_mod,
    pub dropfn: Fn_dropfn,
    pub clonefn: Fn_appstate_clone,
    pfield: [u8;0]
}
unsafe impl Send for AppStateRaw {}
unsafe impl Sync for AppStateRaw {}
impl AppStateRaw {
    pub unsafe fn new(data: *mut c_void, modinit_notify: Fn_modinit_notify, register_mod: Fn_register_mod, dropfn: Fn_dropfn, clonefn: Fn_appstate_clone) -> Self {
        Self { data, modinit_notify, register_mod, dropfn, clonefn, pfield: [] }
    }
}

#[repr(C)]
pub struct NotifStateRaw {
    pub data: *mut c_void,
    pub notify_initmod: Fn_notify_initmod,
    pub notify_deinitmod: Fn_notify_deinitmod,
    pub dropfn: Fn_dropfn,
    pfield: [u8;0]
}
unsafe impl Send for NotifStateRaw {}
unsafe impl Sync for NotifStateRaw {}
impl NotifStateRaw {
    pub unsafe fn new(data: *mut c_void, notify_initmod: Fn_notify_initmod, notify_deinitmod: Fn_notify_deinitmod, dropfn: Fn_dropfn) -> Self {
        Self { data, notify_initmod, notify_deinitmod, dropfn, pfield: [] }
    }
}

#[repr(C)]
pub struct NotifHandleRaw {
    pub data: *mut c_void,
    pub deinit: Fn_deinit,
    pfield: [u8;0]
}
unsafe impl Send for NotifHandleRaw {}
unsafe impl Sync for NotifHandleRaw {}
impl NotifHandleRaw {
    pub unsafe fn new(data: *mut c_void, deinit: Fn_deinit) -> Self {
        Self { data, deinit, pfield: [] }
    }
}

#[repr(C)]
pub struct ModHandleRaw {
    pub data: *mut c_void,
    pub deinit: Fn_deinit,
    pfield: [u8;0]
}
unsafe impl Send for ModHandleRaw {}
unsafe impl Sync for ModHandleRaw {}
impl ModHandleRaw {
    pub unsafe fn new(data: *mut c_void, deinit: Fn_deinit) -> Self {
        Self { data, deinit, pfield: [] }
    }
}

#[repr(C)]
pub struct FeatureRaw {
    pub name_ptr: *const u8,
    pub name_size: usize,
    pub param: u64,
    pfield: [u8;0]
}
unsafe impl Send for FeatureRaw {}
unsafe impl Sync for FeatureRaw {}
impl FeatureRaw {
    pub unsafe fn new(name_ptr: *const u8, name_size: usize, param: u64) -> Self {
        Self { name_ptr, name_size, param, pfield: [] }
    }
}

#[repr(C)]
pub struct ModStateRaw {
    pub name_ptr: *const u8,
    pub name_size: usize,
    pub data: *mut c_void,
    pub get_features: Fn_get_features,
    pub call_func: Fn_call_func,
    pub dropfn: Fn_dropfn,
    pub clonefn: Fn_modstate_clone,
    pfield: [u8;0]
}
unsafe impl Send for ModStateRaw {}
unsafe impl Sync for ModStateRaw {}
impl ModStateRaw {
    pub unsafe fn new(name_ptr: *const u8, name_size: usize, data: *mut c_void, get_features: Fn_get_features, call_func: Fn_call_func, dropfn: Fn_dropfn, clonefn: Fn_modstate_clone) -> Self {
        Self { name_ptr, name_size, data, get_features, call_func, dropfn, clonefn, pfield: [] }
    }
}

#[repr(C)]
pub struct FeatureArrayRaw {
    pub ptr: *const FeatureRaw,
    pub size: usize,
    pub data: *mut c_void,
    pub dropfn: Fn_dropfn,
    pfield: [u8;0]
}
unsafe impl Send for FeatureArrayRaw {}
unsafe impl Sync for FeatureArrayRaw {}
impl FeatureArrayRaw {
    pub unsafe fn new(ptr: *const FeatureRaw, size: usize, data: *mut c_void, dropfn: Fn_dropfn) -> Self {
        Self { ptr, size, data, dropfn, pfield: [] }
    }
}

#[derive(Copy,Clone)]
#[repr(C)]
pub struct SomeValueRaw {
    pub type_name_ptr: *const u8,
    pub type_name_size: usize,
    pub data: *mut c_void,
    pub dropdata: *mut c_void,
    pub dropfn: Fn_dropfn,
    pfield: [u8;0]
}
unsafe impl Send for SomeValueRaw {}
unsafe impl Sync for SomeValueRaw {}
impl SomeValueRaw {
    pub unsafe fn new(type_name_ptr: *const u8, type_name_size: usize, data: *mut c_void, dropdata: *mut c_void, dropfn: Fn_dropfn) -> Self {
        Self { type_name_ptr,type_name_size, data, dropdata, dropfn, pfield: [] }
    }
}
