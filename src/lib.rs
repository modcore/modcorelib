#[allow(non_camel_case_types)]

mod modcore_primitives;
pub use modcore_primitives::*;
use std::collections::HashMap;
use std::sync::{RwLock,Arc};
use std::any::{Any,TypeId};
use std::mem;
use std::panic::{catch_unwind,UnwindSafe};

struct ForceUnwindSafe<T>{
    obj: T
}
impl<T> UnwindSafe for ForceUnwindSafe<T> {}

fn catcherrs<F: FnOnce() -> R,R>(func: F) -> R {
    let fus = ForceUnwindSafe{obj: func};
    let closure = move|| -> R {
        (fus.obj)()
    };
    match catch_unwind(closure) {
        Ok(retval) => {
            retval
        },
        Err(aaaa) => {
            eprintln!("-!- UNHANDLED PANIC -!-");
            eprintln!("-!- A MOD PANICKED WITH '{}' -!-",{
                match aaaa.downcast::<String>() {
                    Ok(aaaa) => {
                        aaaa.to_string()
                    },
                    Err(aaaa) => {
                        match aaaa.downcast::<&str>() {
                            Ok(aaaa) => {
                                (&*aaaa).to_string()
                            },
                            Err(_aaaa) => {
                                "<unknown error>".to_string()
                            }
                        }
                    }
                }
            });
            eprintln!("-!- ABORTING THE PROCESS -!-");
            std::process::abort();
        }
    }
}

#[repr(transparent)]
pub struct AppState {
    raw: AppStateRaw
}

#[repr(transparent)]
pub struct ModHandle {
    raw: ModHandleRaw
}

#[repr(transparent)]
pub struct ModState {
    raw: ModStateRaw
}

#[repr(transparent)]
pub struct SomeValue {
    raw: SomeValueRaw
}

pub struct SomeValueTypes {
    maps: RwLock<(HashMap<String,TypeId>,HashMap<TypeId,String>)>,
}

#[repr(transparent)]
pub struct NotifHandle {
    raw: NotifHandleRaw
}


impl From<AppStateRaw> for AppState {
    fn from(raw: AppStateRaw) -> Self {
        Self { raw }
    }
}

impl From<ModHandleRaw> for ModHandle {
    fn from(raw: ModHandleRaw) -> Self {
        Self { raw }
    }
}

impl From<ModStateRaw> for ModState {
    fn from(raw: ModStateRaw) -> Self {
        Self { raw }
    }
}

impl From<SomeValueRaw> for SomeValue {
    fn from(raw: SomeValueRaw) -> Self {
        Self { raw }
    }
}

impl From<NotifHandleRaw> for NotifHandle {
    fn from(raw: NotifHandleRaw) -> Self {
        Self{ raw }
    }
}


impl From<AppState> for AppStateRaw {
    fn from(cooked: AppState) -> Self {
        unsafe { std::mem::transmute::<AppState,Self>(cooked) }
    }
}

impl From<ModHandle> for ModHandleRaw {
    fn from(cooked: ModHandle) -> Self {
        unsafe { std::mem::transmute::<ModHandle,Self>(cooked) }
    }
}

impl From<ModState> for ModStateRaw {
    fn from(cooked: ModState) -> Self {
        unsafe { std::mem::transmute::<ModState,Self>(cooked) }
    }
}

impl From<SomeValue> for SomeValueRaw {
    fn from(cooked: SomeValue) -> Self {
        unsafe { std::mem::transmute::<SomeValue,Self>(cooked) }
    }
}

impl From<NotifHandle> for NotifHandleRaw {
    fn from(cooked: NotifHandle) -> Self {
        unsafe { std::mem::transmute::<NotifHandle,Self>(cooked) }
    }
}


impl From<&AppState> for &AppStateRaw {
    fn from(cooked: &AppState) -> Self {
        unsafe { std::mem::transmute::<&AppState,Self>(cooked) }
    }
}

impl From<&ModHandle> for &ModHandleRaw {
    fn from(cooked: &ModHandle) -> Self {
        unsafe { std::mem::transmute::<&ModHandle,Self>(cooked) }
    }
}

impl From<&ModState> for &ModStateRaw {
    fn from(cooked: &ModState) -> Self {
        unsafe { std::mem::transmute::<&ModState,Self>(cooked) }
    }
}

impl From<&SomeValue> for &SomeValueRaw {
    fn from(cooked: &SomeValue) -> Self {
        unsafe { std::mem::transmute::<&SomeValue,Self>(cooked) }
    }
}

impl From<&NotifHandle> for &NotifHandleRaw {
    fn from(cooked: &NotifHandle) -> Self {
        unsafe { std::mem::transmute::<&NotifHandle,Self>(cooked) }
    }
}


impl From<&AppStateRaw> for &AppState {
    fn from(raw: &AppStateRaw) -> Self {
        unsafe { std::mem::transmute::<&AppStateRaw,Self>(raw) }
    }
}

impl From<&ModHandleRaw> for &ModHandle {
    fn from(raw: &ModHandleRaw) -> Self {
        unsafe { std::mem::transmute::<&ModHandleRaw,Self>(raw) }
    }
}

impl From<&ModStateRaw> for &ModState {
    fn from(raw: &ModStateRaw) -> Self {
        unsafe { std::mem::transmute::<&ModStateRaw,Self>(raw) }
    }
}

impl From<&SomeValueRaw> for &SomeValue {
    fn from(raw: &SomeValueRaw) -> Self {
        unsafe { std::mem::transmute::<&SomeValueRaw,Self>(raw) }
    }
}

impl From<&NotifHandleRaw> for &NotifHandle {
    fn from(raw: &NotifHandleRaw) -> Self {
        unsafe { std::mem::transmute::<&NotifHandleRaw,Self>(raw) }
    }
}


impl Drop for ModHandle {
    fn drop(&mut self) {
        unsafe {(self.raw.deinit)(self.raw.data);};
    }
}

impl Drop for NotifHandle {
    fn drop(&mut self) {
        unsafe {(self.raw.deinit)(self.raw.data);};
    }
}

#[derive(Clone)]
struct MyModStateData {
    data: Arc<dyn Any + Send + Sync>,
    name: Arc<String>,
    call_func: Arc<dyn Fn(Arc<dyn Any + Send + Sync>,&str,SomeValue) -> SomeValue + Send + Sync>,
    get_features: Arc<dyn Fn(Arc<dyn Any + Send + Sync>) -> HashMap<String,u64> + Send + Sync>
}

unsafe extern "C" fn mymod_call_func(rawdata: *mut c_void, name_ptr: *const u8, name_size: usize, arg: SomeValueRaw) -> SomeValueRaw {
    let data = &*(rawdata as *mut MyModStateData);
    let name = catcherrs(||{std::str::from_utf8(std::slice::from_raw_parts(name_ptr,name_size)).unwrap()});
    let retval=catcherrs(||{(data.call_func)(data.data.clone(),name,arg.into())});
    let retval_raw = retval.raw;
    mem::forget(retval);
    retval_raw
}

struct MyFeatureArrayData {
    real: HashMap<String,u64>,
    fts: Vec<FeatureRaw>
}

unsafe extern "C" fn mymod_ftar_dropfn(rawdata: *mut c_void) {
    let _data = Box::from_raw(rawdata as *mut MyFeatureArrayData);
}

unsafe extern "C" fn mymod_get_features(rawdata: *mut c_void) -> FeatureArrayRaw {
    let data = &*(rawdata as *mut MyModStateData);
    let hmm = catcherrs(||{(data.get_features)(data.data.clone())});
    let mut mfad = Box::new(MyFeatureArrayData { real: hmm, fts: Vec::new() });
    for (name,param) in &mfad.real {
        mfad.fts.push(FeatureRaw::new(name.as_ptr(),name.len(),*param));
    }
    FeatureArrayRaw::new(mfad.fts.as_ptr(),mfad.fts.len(),Box::into_raw(mfad) as *mut c_void,mymod_ftar_dropfn)
}

unsafe extern "C" fn mymod_dropfunc(rawdata: *mut c_void) {
    let _data = Arc::from_raw(rawdata as *mut MyModStateData);
}

unsafe extern "C" fn mymod_clone(rawms: *const ModStateRaw) -> ModStateRaw {
    let msr = &*rawms;
    let name_ptr = msr.name_ptr;
    let name_size = msr.name_size; 
    let data = Box::into_raw(Box::new((*(msr.data as *mut MyModStateData)).clone())) as *mut c_void;
    ModStateRaw::new(name_ptr,name_size,data,msr.get_features,msr.call_func,msr.dropfn,msr.clonefn)
}

pub enum ModNotif<'a> {
    Init(&'a ModState),
    Deinit(&'a str)
}

struct MyNotifStateData {
    data: Arc<dyn Any + Send + Sync>,
    notif_fn: Box<dyn Fn(Arc<dyn Any +Send + Sync>,ModNotif)>
}

unsafe extern "C" fn mynotif_notify_initmod(rawdata: *mut c_void, ms: *const ModStateRaw) {
    let data = &*(rawdata as *mut MyNotifStateData);
    let ms = &*ms;
    let msc = ModStateRaw::new(ms.name_ptr,ms.name_size,ms.data,ms.get_features,ms.call_func,ms.dropfn,ms.clonefn).into();
    catcherrs(||{(data.notif_fn)(data.data.clone(),ModNotif::Init(&msc))});
    mem::forget(ms);
}

unsafe extern "C" fn mynotif_notify_deinitmod(rawdata: *mut c_void, name_ptr: *const u8, name_size: usize) {
    let data = &*(rawdata as *mut MyNotifStateData);
    let name = catcherrs(||{std::str::from_utf8(std::slice::from_raw_parts(name_ptr,name_size)).unwrap()});
    catcherrs(||{(data.notif_fn)(data.data.clone(),ModNotif::Deinit(name))});
}

unsafe extern "C" fn mynotif_dropfn(rawdata: *mut c_void) {
    let _data = Box::from_raw(rawdata as *mut MyNotifStateData);
}

struct MyAppStateDataReal {
    mods: HashMap<String,ModStateRaw>,
    notifs: HashMap<u64,NotifStateRaw>,
    counter: u64
}

type MyAppStateDataInner = RwLock<MyAppStateDataReal>;
type MyAppStateData = Arc<MyAppStateDataInner>;

struct MyNotifHandleData {
    id: u64,
    rawdata: *mut c_void
}

unsafe extern "C" fn mynotifhandle_deinit(rawdata: *mut c_void) {
    let data = Box::from_raw(rawdata as *mut MyNotifHandleData);
    let adata = Arc::from_raw(data.rawdata as *mut MyAppStateDataInner);
    let mer = Arc::clone(&adata);
    let mut me = catcherrs(||{mer.write().unwrap()});
    Arc::into_raw(adata);
    let ns = catcherrs(||{me.notifs.remove(&data.id).unwrap()});
    (ns.dropfn)(ns.data);
}

unsafe extern "C" fn myapp_modinit_notify(rawdata: *mut c_void, ns: NotifStateRaw) -> NotifHandleRaw {
    let data = Arc::from_raw(rawdata as *mut MyAppStateDataInner);
    let mer = Arc::clone(&data);
    let mut me = catcherrs(||{mer.write().unwrap()});
    Arc::into_raw(data);
    for (_name,msr) in &me.mods {
        (ns.notify_initmod)(ns.data,msr as *const ModStateRaw);
    }
    let counter = me.counter;
    me.notifs.insert(counter,ns);
    me.counter=me.counter+1;
    NotifHandleRaw::new(Box::into_raw(Box::new(MyNotifHandleData{id: counter,rawdata})) as *mut c_void,mynotifhandle_deinit)
}

struct MyModHandleData {
    id: String,
    rawdata: *mut c_void
}

unsafe extern "C" fn mymodhandle_deinit(rawdata: *mut c_void) {
    let data = Box::from_raw(rawdata as *mut MyModHandleData);
    let adata = Arc::from_raw(data.rawdata as *mut MyAppStateDataInner);
    let mer = Arc::clone(&adata);
    let mut me = catcherrs(||{mer.write().unwrap()});
    Arc::into_raw(adata);
    let ms = catcherrs(||{me.mods.remove(&data.id).unwrap()});
    (ms.dropfn)(ms.data);
    let name_ptr = data.id.as_ptr();
    let name_size = data.id.len();
    for (_id,nsr) in &me.notifs {
        (nsr.notify_deinitmod)(nsr.data,name_ptr,name_size);
    }
}

unsafe extern "C" fn myapp_register_mod(rawdata: *mut c_void, ms: ModStateRaw) -> ModHandleRaw {
    let data = Arc::from_raw(rawdata as *mut MyAppStateDataInner);
    let mer = Arc::clone(&data);
    let mut me = catcherrs(||{mer.write().unwrap()});
    Arc::into_raw(data);
    for (_id,nsr) in &me.notifs {
        (nsr.notify_initmod)(nsr.data,&ms as *const ModStateRaw);
    }
    let name = catcherrs(||{std::str::from_utf8(std::slice::from_raw_parts(ms.name_ptr,ms.name_size)).unwrap().to_owned()});
    if me.mods.contains_key(&name) {
        catcherrs(||{panic!("mod with this name already registered: {}",name.as_str())});
    }
    me.mods.insert(name.clone(),ms);
    ModHandleRaw::new(Box::into_raw(Box::new(MyModHandleData { id: name, rawdata })) as *mut c_void,mymodhandle_deinit)
}

unsafe extern "C" fn myapp_dropfn(rawdata: *mut c_void) {
    let _data = Arc::from_raw(rawdata as *mut MyAppStateDataInner);
}

unsafe extern "C" fn myapp_clone(rawas: *const AppStateRaw) -> AppStateRaw {
    let rawas = &*(rawas);
    let data = Arc::into_raw({let aa=Arc::from_raw(rawas.data as *mut MyAppStateDataInner);let bb=aa.clone();Arc::into_raw(aa);bb}) as *mut c_void;
    AppStateRaw::new(data,rawas.modinit_notify,rawas.register_mod,rawas.dropfn,rawas.clonefn)
}

impl AppState {
    pub fn new() -> Self {
        let datar = Arc::new(RwLock::new(MyAppStateDataReal { mods: HashMap::new(), notifs: HashMap::new(), counter: 0 }));
        let data = Arc::into_raw(datar) as *mut MyAppStateData as *mut c_void;
        unsafe {AppStateRaw::new(data,myapp_modinit_notify,myapp_register_mod,myapp_dropfn,myapp_clone)}.into()
    }

    pub fn register_mod(&self, name: String, data: Arc<dyn Any + Send + Sync>, call_func: Arc<dyn Fn(Arc<dyn Any + Send + Sync>, &str, SomeValue) -> SomeValue + Send + Sync>, get_features: Arc<dyn Fn(Arc<dyn Any + Send + Sync>) -> HashMap<String,u64> + Send + Sync>) -> ModHandle {
        let mmmsd = Box::new(MyModStateData { data, name: Arc::new(name), call_func, get_features });
        let rawms = unsafe { ModStateRaw::new( mmmsd.name.as_ptr(), mmmsd.name.len(), Box::into_raw(mmmsd) as *mut c_void, mymod_get_features, mymod_call_func, mymod_dropfunc, mymod_clone) };
        unsafe{(self.raw.register_mod)(self.raw.data,rawms)}.into()
    }

    pub fn modinit_notify(&self, data: Arc<dyn Any + Send + Sync>, notif_fn: Box<dyn Fn(Arc<dyn Any + Send + Sync>,ModNotif) + Send + Sync>) -> NotifHandle {
        let mnsd = Box::new(MyNotifStateData { data, notif_fn });
        let rawns = unsafe { NotifStateRaw::new( Box::into_raw(mnsd) as *mut c_void, mynotif_notify_initmod, mynotif_notify_deinitmod, mynotif_dropfn ) };
        unsafe{(self.raw.modinit_notify)(self.raw.data,rawns)}.into()
    }
}

impl ModState {
    pub fn get_name<'a>(&'a self) -> &'a str {
        std::str::from_utf8(unsafe{std::slice::from_raw_parts(self.raw.name_ptr,self.raw.name_size)}).unwrap()
    }
    pub fn call(&self, name: &str, arg: SomeValue) -> SomeValue {
        let argraw = arg.raw;
        mem::forget(arg);
        unsafe{(self.raw.call_func)(self.raw.data,name.as_ptr(),name.len(),argraw)}.into()
    }
    pub fn get_features(&self) -> HashMap<String,u64> {
        let ftar = unsafe{(self.raw.get_features)(self.raw.data)};
        let ftsl: &[FeatureRaw] = unsafe {
            std::slice::from_raw_parts(ftar.ptr,ftar.size)
        };
        let mut ftmp = HashMap::new();
        for ft in ftsl {
            ftmp.insert(String::from(std::str::from_utf8(unsafe { std::slice::from_raw_parts(ft.name_ptr,ft.name_size) }).unwrap()),ft.param);
        }
        unsafe{(ftar.dropfn)(ftar.data);};
        ftmp
    }
}

impl Clone for ModState {
    fn clone(&self) -> Self {
        unsafe{(self.raw.clonefn)(&self.raw as *const ModStateRaw)}.into()
    }
}

impl Drop for ModState {
    fn drop(&mut self) {
        unsafe{(self.raw.dropfn)(self.raw.data);};
    }
}

impl Clone for AppState {
    fn clone(&self) -> Self {
        unsafe{(self.raw.clonefn)(&self.raw as *const AppStateRaw)}.into()
    }
}

impl Drop for AppState {
    fn drop(&mut self) {
        unsafe{(self.raw.dropfn)(self.raw.data);};
    }
}

#[allow(dead_code)]
struct MySomeValueDropData {
    type_name: String,
    val: Box<dyn Any + Send + Sync>
}

extern "C" fn mysomevalue_dropfn(dropdata: *mut c_void) {
    let dropdata = unsafe { Box::from_raw(dropdata as *mut MySomeValueDropData) };
    mem::drop(dropdata);
}

extern "C" fn mysomevalue_nulldropfn(_dropdata: *mut c_void) {
}

enum MyNullType {}

impl SomeValueTypes {
    pub fn new() -> Self {
        let svt=Self { 
            maps: RwLock::new((HashMap::new(),HashMap::new()))
        };
        unsafe {svt.register_type("null".to_owned(),TypeId::of::<MyNullType>());};
        svt
    }
    
    pub unsafe fn register_type(&self,type_name: String, type_id: TypeId) {
        let mut lmaps = self.maps.write().unwrap();
        let (str_to_type,type_to_str) = &mut *lmaps;
        if str_to_type.contains_key(&type_name) || type_to_str.contains_key(&type_id) {
            panic!("typeid or typename already registered");
        } else {
            let type_name2 = type_name.clone();
            str_to_type.insert(type_name,type_id);
            type_to_str.insert(type_id,type_name2);
        }
    }

    pub fn types_match<T: Any + Send + Sync>(&self,val: &SomeValue) -> bool {
        let lmaps = self.maps.read().unwrap();
        let (str_to_type,_type_to_str) = &*lmaps;
        let typeid = TypeId::of::<T>();
        let type_name = unsafe {
            std::str::from_utf8(std::slice::from_raw_parts(val.raw.type_name_ptr,val.raw.type_name_size)).unwrap()
        };
        let stypeid = *(match str_to_type.get(type_name) {
            Some(stpi) => { stpi },
            None => { return false; },
        });
        typeid==stypeid
    }

    pub fn downcast_ref<'a,T: Any + Send + Sync>(&'_ self, val: &'a SomeValue) -> Option<&'a T> {
        if self.types_match::<T>(val) {
            Some(unsafe { &(*(val.raw.data as *mut T)) })
        } else {
            None
        }
    }

    pub fn downcast_mut<'a,T: Any + Send + Sync>(&'_ self, val: &'a mut SomeValue) -> Option<&'a mut T> {
        if self.types_match::<T>(val as &SomeValue) {
            Some(unsafe { &mut (*(val.raw.data as *mut T)) })
        } else {
            None
        }
    }

    pub fn make_val<T: Any + Send + Sync>(&self, mut val: T) -> SomeValue {
        let lmaps = self.maps.read().unwrap();
        let (_str_to_type,type_to_str) = &*lmaps;
        let typeid = TypeId::of::<T>();
        let type_name = type_to_str.get(&typeid).expect("type not registered").clone();
        let ptr = &mut val as *mut T as *mut c_void;
        let dropdata = Box::new(MySomeValueDropData { type_name, val: Box::new(val) });
        let type_name_ptr = dropdata.type_name.as_ptr();
        let type_name_size = dropdata.type_name.len();
        let dropdata_ptr = Box::into_raw(dropdata) as *mut c_void;
        unsafe { SomeValueRaw::new(type_name_ptr,type_name_size,ptr,dropdata_ptr,mysomevalue_dropfn).into() }
    }

    pub fn make_null(&self) -> SomeValue {
        let type_name = "null";
        let type_name_ptr = type_name.as_ptr();
        let type_name_size = type_name.len();
        let ptr = std::ptr::null_mut();
        let dropdata_ptr = std::ptr::null_mut();
        unsafe { SomeValueRaw::new(type_name_ptr,type_name_size,ptr,dropdata_ptr,mysomevalue_nulldropfn).into() }
    }
}

impl Drop for SomeValue {
    fn drop(&mut self) {
        unsafe {(self.raw.dropfn)(self.raw.dropdata);}
    }
}
